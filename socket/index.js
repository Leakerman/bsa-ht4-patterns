import game from "./game"

export default io => {
  io.on("connection", socket => {
    game(socket, io)
  });
};
