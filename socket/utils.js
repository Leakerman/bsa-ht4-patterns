export const getCurrentRoomName = (socket, rooms) => {
  return Object.keys(socket.rooms).find((roomName) => rooms.has(roomName));
};

export const roomsArr = (rooms) => {
  return Array.from(rooms)
    .map(([k, v]) => ({ name: k, ...v }))
    .filter((room) => !room.isStarted);
};

export function isAllPlayersFinished(players) {
  return players.reduce((isEveryoneFinished, player) => {
    return !!player.finishTime && isEveryoneFinished;
  }, !!players.length);
}

export function createLeaderboardFromPlayers(players) {
  const playersFinished = players
    .filter((p) => !!p.finishTime)
    .sort((a, b) => b.finishTime - a.finishTime);
  const playersNotFinished = players
    .filter((p) => !p.finishTime)
    .sort((a, b) => b.symbolsTyped - a.symbolsTyped);
  const places = [...playersFinished, ...playersNotFinished];
  return places
    .map((player, index) => `${index + 1}) ${player.username}`)
    .join("\n");
}

export function socketInRoom(socket,eventName,roomName, data) {
  socket.to(roomName).emit(eventName, data)
  socket.emit(eventName, data)
}