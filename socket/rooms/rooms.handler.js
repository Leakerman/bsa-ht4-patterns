import { getCurrentRoomName, roomsArr, isAllPlayersFinished } from "../utils";
import { MAXIMUM_USERS_FOR_ONE_ROOM } from "../config";
import { texts } from "../../data";
import { comentator } from "../game";

export default ({ socket, io, rooms, users, username }) => {
  socket.on("CREATE_ROOM", (roomName) => {
    if (!createRoom(socket, roomName, rooms)) return;
    joinRoom(socket, roomName, username, rooms);
  });

  socket.on("JOIN_ROOM", (roomName) => {
    joinRoom(socket, roomName, username, rooms);
  });

  socket.on("BACK_TO_ROOMS", () => {
    leaveRoom(socket, username, rooms);
  });

  socket.on("disconnect", () => {
    leaveRoom(socket, username, rooms);
    users.splice(users.indexOf(username), 1);
  });

  socket.on("USER_TOGGLE_READINESS", (username) => {
    const roomName = getCurrentRoomName(socket, rooms);
    const roomInfo = rooms.get(roomName);
    userToggleReadiness(username, roomName, rooms);

    io.in(roomName).emit("UPDATE_ROOM", {
      name: roomName,
      ...roomInfo,
    });
    checkForAllPlayersReady(socket, rooms, roomName);
  });
};

function userToggleReadiness(username, roomName, rooms) {
  const roomInfo = rooms.get(roomName);
  const player = roomInfo.players.find((p) => p.username === username);
  player.ready = !player.ready;
}

function createRoom(socket, roomName, rooms) {
  if (!roomName || rooms.has(roomName)) {
    socket.emit("CREATE_ROOM_FAILED", roomName);
    return false;
  }
  rooms.set(roomName, {
    isStarted: false,
    players: [],
    text: "",
  });
  return true;
}

function joinRoom(socket, roomName, username, rooms) {
  if (!addUserToRoomMap(username, roomName, rooms)) {
    socket.emit("JOIN_ROOM_FAILED");
    return;
  }
  socket.join(roomName, () => {
    const roomInfo = rooms.get(roomName);
    socket.broadcast.emit("UPDATE_ROOMS", roomsArr(rooms));
    socket.emit("JOIN_ROOM_DONE", {
      name: roomName,
      ...roomInfo,
    });

    socket.to(roomName).emit("UPDATE_ROOM", {
      name: roomName,
      ...roomInfo,
    });
    socket.emit("UPDATE_ROOM", {
      name: roomName,
      ...roomInfo,
    });
  });
}

function leaveRoom(socket, username, rooms) {
  let roomName = "";
  for (let [name, { players }] of rooms) {
    const playerCandidateIndex = players.findIndex(
      (p) => p.username === username
    );
    if (playerCandidateIndex !== -1) {
      roomName = name;
      break;
    }
  }

  const roomInfo = rooms.get(roomName);
  if (!removeUserFromRoomMap(username, roomName, rooms)) {
    rooms.delete(roomName);
  } else {
    socket.to(roomName).emit("UPDATE_ROOM", { roomName, ...roomInfo });
    checkForAllPlayersReady(socket, rooms, roomName);
  }

  if (isAllPlayersFinished(roomInfo.players)) {
    comentator.notify("GAME_OVER", { roomName, players: roomInfo.players });

    socket.to(roomName).emit("GAME_OVER", roomInfo.players);
    socket.emit("GAME_OVER", roomInfo.players);
  }

  socket.broadcast.emit("UPDATE_ROOMS", roomsArr(rooms));
  socket.emit("UPDATE_ROOMS", roomsArr(rooms));

  socket.leave(roomName);
}

function addUserToRoomMap(username, roomName, rooms) {
  if (rooms.get(roomName).players.length >= MAXIMUM_USERS_FOR_ONE_ROOM) {
    return false;
  }
  rooms
    .get(roomName)
    .players.push({ username, isReady: false, finishTime: 0, symbolsTyped: 0 });
  return true;
}

function removeUserFromRoomMap(username, roomName, rooms) {
  const players = rooms.get(roomName).players;
  const playerIndex = players.findIndex((p) => p.username === username);
  players.splice(playerIndex, 1);
  if (players.length <= 0) {
    return false;
  }
  return true;
}

function checkForAllPlayersReady(socket, rooms, roomName) {
  const roomInfo = rooms.get(roomName);
  if (roomInfo.isStarted) return;
  const players = roomInfo.players;
  const areReady = players.reduce(
    (areReady, player) => areReady && player.ready,
    !!players.length
  );
  if (areReady) {
    rooms.get(roomName).isStarted = true;

    const textIndex = Math.round(Math.random() * (texts.length - 1));

    socket.to(roomName).emit("PREPARE_TIMER", textIndex);
    socket.emit("PREPARE_TIMER", textIndex);

    socket.broadcast.emit("UPDATE_ROOMS", roomsArr(rooms));
  }
}
