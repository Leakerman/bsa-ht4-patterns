import { SECONDS_FOR_COMENTATOR_SHOW_STATISTICS } from "../config";
import { comentator, rooms } from "../game";
import { createLeaderboardFromPlayers, socketInRoom } from "../utils";

export const comentatorOnStart = ({ players, name }) => {
  comentator.subscribe("EVERY_N_SECONDS_STATISTICS", comentatorOnShowStatistics);
  comentator.subscribe("PLAYER_ALMOST_FINISHED", comentatorOnAlmostFinished);
  comentator.subscribe("PLAYER_FINISH", comentatorOnPlayerFinished);
  comentator.subscribe("GAME_OVER", comentatorOnGameOver);

  const playersList = players.map((p) => `1) ${p.username}`).join("\n");
  socketInRoom(
    comentator.socket,
    "COMENTATOR_SAY",
    name,
    `А тим часом список гонщиків:\n${playersList}`
  );

  comentator.notify("EVERY_N_SECONDS_STATISTICS", {
    frequency: SECONDS_FOR_COMENTATOR_SHOW_STATISTICS * 1000,
    roomName: name,
  });
};

// default frequency: 30 seconds
const comentatorOnShowStatistics = ({ frequency = 30 * 1000, roomName }) => {
  setTimeout(() => {
    const players = rooms.get(roomName).players;
    socketInRoom(
      comentator.socket,
      "COMENTATOR_SAY",
      roomName,
      `Кожно-${frequency / 1000}-cекунднi новини, наразi маемо таку ситуацiю на трасi:\n
      ${createLeaderboardFromPlayers(players)}`
    );

    comentator.notify("EVERY_N_SECONDS_STATISTICS", {
      frequency,
      roomName,
    });
  }, frequency);
};

const comentatorOnAlmostFinished = ({ username, roomName }) => {
  socketInRoom(
    comentator.socket,
    "COMENTATOR_SAY",
    roomName,
    `До фінішу залишилось зовсім небагато і схоже що першим його може перетнути ${username} з команди Atom на своєму білому BMW. `
  );

  comentator.unsubscribe("PLAYER_ALMOST_FINISHED");
};

const comentatorOnPlayerFinished = ({ username, roomName }) => {
  socketInRoom(
    comentator.socket,
    "COMENTATOR_SAY",
    roomName,
    `Фінішну пряму пересікає ${username}`
  );
};

const comentatorOnGameOver = ({ roomName , players}) => {
  comentator.unsubscribe("EVERY_N_SECONDS_STATISTICS");
  comentator.unsubscribe("PLAYER_FINISH");
  comentator.unsubscribe("GAME_OVER");

  socketInRoom(
    comentator.socket,
    "COMENTATOR_SAY",
    roomName,
    `Гра закiнчилась, i фiнальний результат:\n${createLeaderboardFromPlayers(players)}`
  );
};
