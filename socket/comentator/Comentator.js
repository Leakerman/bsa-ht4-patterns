/////////////////OBSERVER///////////////
export class Comentator {
  constructor(entries, socket) {
    this.listeners = new Map(entries);
    this.socket = socket;
  }

  subscribe(eventType, listener) {
    this.listeners.set(eventType, listener);
  }

  unsubscribe(eventType) {
    this.listeners.delete(eventType);
  }

  notify(eventType, data) {
    const listener = this.listeners.get(eventType);
    if (listener) {
      listener(data);
    }
  }
}
