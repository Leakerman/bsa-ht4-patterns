import {
  SECONDS_TIMER_BEFORE_START_GAME,
  SECONDS_FOR_GAME,
  LETTERS_TO_FINISH_FOR_COMENTATOR,
} from "./config";
import roomsHandler from "./rooms/rooms.handler";
import { roomsArr, getCurrentRoomName, isAllPlayersFinished } from "./utils";
import { Comentator } from "./comentator/Comentator";
import { comentatorOnStart } from "./comentator/listeners";

const users = [];
export const rooms = new Map();
export let comentator;

export default (socket, io) => {
  const username = socket.handshake.query.username;
  if (users.includes(username)) {
    socket.emit("USERNAME_ALREADY_TAKEN", username);
    return;
  } else {
    users.push(username);
    socket.emit("UPDATE_ROOMS", roomsArr(rooms));
    comentator = new Comentator([["START", comentatorOnStart]], socket);
  }

  roomsHandler({ socket, io, rooms, users, username });

  socket.on("USER_TYPED_SYMBOL", ({ userTyped, secondsLeft }) => {
    const roomName = getCurrentRoomName(socket, rooms);
    const roomInfo = rooms.get(roomName);
    const currPlayer = roomInfo.players.find((p) => p.username === userTyped);
    currPlayer.symbolsTyped += 1;

    if (
      roomInfo.text.length - currPlayer.symbolsTyped <=
      LETTERS_TO_FINISH_FOR_COMENTATOR
    ) {
      comentator.notify("PLAYER_ALMOST_FINISHED", { username, roomName });
    }

    if (currPlayer.symbolsTyped === roomInfo.text.length) {
      currPlayer.finishTime = secondsLeft;
      comentator.notify("PLAYER_FINISH", { username, roomName });
      if (isAllPlayersFinished(roomInfo.players)) {
        io.in(roomName).emit("GAME_OVER", roomInfo.players);
        comentator.notify("GAME_OVER", { roomName, players: roomInfo.players });

        return;
      }
    }
    io.in(roomName).emit("UPDATE_ROOM", { name: roomName, ...roomInfo });
  });

  socket.on("TIME_OVER", () => {
    const roomName = getCurrentRoomName(socket, rooms);
    const { players } = rooms.get(roomName);
    socket.emit("GAME_OVER");
    comentator.notify("GAME_OVER", { roomName, players });
  });

  socket.on("SET_DEFAULT_ROOM", () => {
    const roomName = getCurrentRoomName(socket, rooms);
    const roomInfo = rooms.get(roomName);
    roomInfo.players.forEach((player) => {
      player.finishTime = 0;
      player.symbolsTyped = 0;
      player.ready = false;
    });
    roomInfo.isStarted = false;
    roomInfo.text = "";

    io.in(roomName).emit("UPDATE_ROOM", {
      name: roomName,
      ...roomInfo,
    });

    socket.emit("UPDATE_ROOMS", roomsArr(rooms));
    socket.broadcast.emit("UPDATE_ROOMS", roomsArr(rooms));
  });

  socket.on("START_TIMER", (text) => {
    const msBeforeStartGame = SECONDS_TIMER_BEFORE_START_GAME * 1000;
    let secondsBeforeStart = msBeforeStartGame;

    const roomName = getCurrentRoomName(socket, rooms);
    const roomInfo = rooms.get(roomName);
    roomInfo.text = text;

    const interval = setInterval(() => {
      socket.emit("GAME_START_IN", secondsBeforeStart / 1000);
      secondsBeforeStart -= 1000;
      if (secondsBeforeStart <= 0) {
        socket.emit("START_GAME", SECONDS_FOR_GAME, {
          name: roomName,
          ...roomInfo,
        });
        comentator.notify("START", {
          name: roomName,
          ...roomInfo,
        });
        clearInterval(interval);
      }
    }, 1000);
  });
};
