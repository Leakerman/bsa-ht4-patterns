export const createElement = ({
  tagName,
  className,
  content = "",
  html = "",
  attributes = {},
}) => {
  
  const element = document.createElement(tagName);

  if (className) {
    addClass(element, className);
  }

  if (content.trim()) {
    element.innerText = content;
  }

  if (html.trim()) {
    element.innerHTML = html;
  }

  Object.keys(attributes).forEach((key) =>
    element.setAttribute(key, attributes[key])
  );

  return element;
};

export const addClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.add(...classNames);
};

export const removeClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.remove(...classNames);
};

export const formatClassNames = (className) =>
  className.split(" ").filter(Boolean);
