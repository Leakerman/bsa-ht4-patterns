import { socket } from "./game.mjs";
import { addClass, removeClass, createElement } from "./dom.helper.mjs";

const username = sessionStorage.getItem("username");

class GameView {
  constructor() {
    this.readyButton = document.getElementById("ready-button");
    this.timer = document.getElementById("timer");
    this.gameWrap = document.getElementById("text");
    this.gameText = document.getElementById("game-text");
    this.gameTimer = document.getElementById("time-left");
    this.backToRoomsButton = document.getElementById("back-to-rooms");
    this.comentator = document.getElementById("comentator");
    this.symbols = null;
  }

  onKeyDownHandler(e) {
    if (e.repeat) return;
    const typed = e.key;
    for (let i = 0; i < this.symbols.length; i++) {
      const symbolToCheck =
        this.symbols[i].innerHTML === "&nbsp;"
          ? " "
          : this.symbols[i].innerHTML;

      if (symbolToCheck === typed) {
        // if typed letter is the one from the word
        if (this.symbols[i].classList.contains("typed")) {
          // if it already has class with the background color then check the next one
          continue;
        } else if (
          (!this.symbols[i].classList.contains("typed") &&
            !this.symbols[i - 1]) ||
          this.symbols[i - 1].classList.contains("typed")
        ) {
          // if it dont have class,
          // if it is not first letter
          // or if the letter before it dont have class
          // (this is done to avoid marking the letters who are not in order for being checked, for example if you have two "A"s so to avoid marking both of them if the first one is at the index 0 and second at index 5 for example)
          removeClass(this.symbols[i], "next");
          if (this.symbols[i + 1]) {
            addClass(this.symbols[i + 1], "next");
          }
          this.symbols[i].classList.add("typed");
          socket.emit("USER_TYPED_SYMBOL", {
            userTyped: username,
            secondsLeft: window.secondsLeft,
          });
          break;
        }
      }
    }
  }

  setComentatorText(text) {
    this.comentator.innerText = text;
  }

  setGameTimer(time) {
    this.gameTimer.innerText = `seconds left ${time}`;
  }

  displayTimer() {
    addClass(this.readyButton, "display-none");
    addClass(this.backToRoomsButton, "display-none");
    removeClass(this.timer, "display-none");
  }

  formGameTextElement(text) {
    this.gameText.innerHTML = "";
    text.split("").forEach((symbol) => {
      const element = createElement({
        tagName: "span",
        className: "span",
        html: symbol === " " ? "&nbsp;" : symbol,
      });
      this.gameText.appendChild(element);
    });
    this.symbols = document.querySelectorAll(".span");

    addClass(this.symbols[0], "next");
  }

  displayGame() {
    addClass(this.timer, "display-none");
    removeClass(this.gameWrap, "display-none");
    document.onkeydown = this.onKeyDownHandler.bind(this);
  }

  setRoomDefault() {
    addClass(this.gameWrap, "display-none");
    removeClass(this.readyButton, "display-none");
    removeClass(this.backToRoomsButton, "display-none");
    document.onkeydown = null;
  }
}

class GameFacade {
  constructor() {
    this.view = new GameView();
  }

  getTextFromServer(textIndex) {
    const theUrl = `http://localhost:3002/game/texts/${textIndex}`;
    const xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false);
    xmlHttp.send(null);
    const text = JSON.parse(xmlHttp.responseText).text;
    window.textLength = text.length;
    return text;
  }

  prepareTimer(textIndex) {
    this.view.displayTimer();

    const text = this.getTextFromServer(textIndex);
    socket.emit("START_TIMER", text);

    this.view.formGameTextElement(text);
  }

  startGame(gameTime) {
    this.view.displayGame();
    window.secondsLeft = gameTime;

    const interval = setInterval(() => {
      this.view.setGameTimer(window.secondsLeft);
      if (window.secondsLeft <= 0) {
        socket.emit("TIME_OVER");
        clearInterval(interval);
        this.view.setGameTimer(gameTime);
        return;
      }
      window.secondsLeft--;
    }, 1000);
    window.interval = interval;
  }

  setDefault() {
    window.textLength = "";
    this.view.setRoomDefault();

    socket.emit("SET_DEFAULT_ROOM");
  }

  gameOver() {
    this.setDefault();
  }

  comentatorSay(text) {
    this.view.setComentatorText(text);
  }
}

export default new GameFacade();
