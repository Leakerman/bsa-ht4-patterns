import RoomHandler from "./rooms.helper.mjs";
import GameHandler from "./game.helper.mjs";

const username = sessionStorage.getItem("username");
if (!username) {
  window.location.replace("/login");
}
export const socket = io("", { query: { username } });

const timer = document.getElementById("timer");

socket.on("PREPARE_TIMER", (textIndex) => GameHandler.prepareTimer(textIndex));
socket.on("GAME_START_IN", (seconds) => { timer.innerText = seconds; });
socket.on("START_GAME", (gameTime) => GameHandler.startGame(gameTime));
socket.on("GAME_OVER", (players) => GameHandler.gameOver(players));
socket.on("COMENTATOR_SAY", (text) => GameHandler.comentatorSay(text));

socket.on("USERNAME_ALREADY_TAKEN", (username) => RoomHandler.usernameAlreadyTaken(username));
socket.on("CREATE_ROOM_FAILED", (roomName) => RoomHandler.createRoomFailed(roomName));
socket.on("UPDATE_ROOMS", (rooms) => RoomHandler.updateRooms(rooms));
socket.on("JOIN_ROOM_DONE", (room) => RoomHandler.joinRoomDone(room));
socket.on("UPDATE_ROOM", (room) => RoomHandler.updateRoom(room));
socket.on("JOIN_ROOM_FAILED", () => alert("The maximum number of players is reached in the room"));
