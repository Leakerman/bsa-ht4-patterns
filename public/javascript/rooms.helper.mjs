import { socket } from "./game.mjs";
import { addClass, removeClass, createElement } from "./dom.helper.mjs";

const username = sessionStorage.getItem("username");

class RoomsView {
  constructor() {
    this.progressContainer = document.getElementById("progress-container");
    this.roomsPage = document.getElementById("rooms-page");
    this.gamePage = document.getElementById("game-page");
    this.gameTitle = document.querySelector("#game-page .title");
    this.roomsContainer = document.getElementById("rooms-container");

    this.readyButton = document.getElementById("ready-button");
    this.createRoomButton = document.getElementById("create-room");
    this.backToRoomsButton = document.getElementById("back-to-rooms");
  }

  getViewItem(name) {
    return this[name];
  }

  setRooms(rooms) {
    this.roomsContainer.innerHTML = "";
    this.roomsContainer.append(...rooms);
  }

  setPlayersProgress(playersProgress) {
    this.progressContainer.innerHTML = "";
    this.progressContainer.append(...playersProgress);
  }

  setUserReadiness(isReady) {
    isReady
      ? (this.readyButton.innerText = "Not ready")
      : (this.readyButton.innerText = "Ready");
  }

  setGameTitle(title) {
    this.gameTitle.innerText = title;
  }

  createRoomItem({ name, players }) {
    const room = createElement({
      tagName: "div",
      className: "room",
      html: `
      <div class="room-connected">Users connected: ${players.length}</div>
      <p class="room-name margin-auto">${name}</p>
      `,
    });

    const joinButton = createElement({
      tagName: "button",
      className: "button margin-auto",
      content: "Join",
    });

    joinButton.addEventListener("click", () => {
      socket.emit("JOIN_ROOM", name);
    });

    room.append(joinButton);

    return room;
  }

  createProgressItem(player) {
    const classes = ["progress"];

    if (player.ready) {
      classes.push("active");
    }

    if (!!player.finishTime) {
      classes.push("finished");
    }

    const playerNick =
      username === player.username
        ? player.username + " (you)"
        : player.username;

    const progressValue = !!window.textLength
      ? (player.symbolsTyped / window.textLength) * 100
      : 0;

    const html = `
        <div class="username">${playerNick}</div>
        <progress class="progressbar" value="${progressValue}" max="100"></progress>
    `;

    return createElement({
      tagName: "div",
      className: classes.join(" "),
      attributes: { id: player.username },
      html,
    });
  }

  displayRoomsPage() {
    addClass(this.gamePage, "display-none");
    removeClass(this.roomsPage, "display-none");
  }

  displayGamePage() {
    addClass(this.roomsPage, "display-none");
    removeClass(this.gamePage, "display-none");
  }
}

class RoomsFacade {
  constructor() {
    this.view = new RoomsView();

    this.view.getViewItem("readyButton").addEventListener("click", () => {
      socket.emit("USER_TOGGLE_READINESS", username);
    });

    this.view.getViewItem("createRoomButton").addEventListener("click", () => {
      socket.emit("CREATE_ROOM", prompt("Enter the room name"));
    });

    this.view.getViewItem("backToRoomsButton").addEventListener("click", () => {
      this.view.displayRoomsPage();
      socket.emit("BACK_TO_ROOMS");
    });
  }

  usernameAlreadyTaken(username) {
    alert(`Username ${username} is already taken`);
    sessionStorage.removeItem("username");
    window.location.replace("/login");
  }

  createRoomFailed(roomName) {
    alert(`Name ${roomName} is already taken`);
  }

  updateRooms(rooms) {
    const allRooms = rooms.map(this.view.createRoomItem);
    this.view.setRooms(allRooms);
  }

  joinRoomDone({ name }) {
    this.view.displayGamePage();
    this.view.setGameTitle(name);
  }

  updateRoom({ players }) {
    const activePlayer = players.find((p) => p.username === username);
    this.view.setUserReadiness(activePlayer.ready);

    const playersProgress = players.map(this.view.createProgressItem);
    this.view.setPlayersProgress(playersProgress);
  }
}

export default new RoomsFacade();